#!/bin/bash

rm -r tmp/
rm -r Output/
mkdir Output
mkdir tmp
mkdir tmp/margins
mkdir tmp/augmented
mkdir tmp/augmented/images
mkdir tmp/augmented/segmentations
mkdir tmp/copypaste_input
mkdir tmp/copypaste_input/foregrounds
mkdir tmp/copypaste_input/backgrounds
