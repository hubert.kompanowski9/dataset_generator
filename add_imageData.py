# -*- coding: utf-8 -*-

import base64
import json
import pathlib
import os
from PIL import Image
import fnmatch

LABELME_PATH = 'tmp/augmented/labelme/'

no_images = len(fnmatch.filter(os.listdir(LABELME_PATH), '*.png'))
no_jsons = len(fnmatch.filter(os.listdir(LABELME_PATH), '*.json'))

if no_images != no_jsons: 
  raise ValueError(f"{LABELME_PATH} has different number of .png and .json files")

for i in range(1, no_images+1):

  try:
    # decode image
    img_path = LABELME_PATH + f'{i}.png'
    encoded = base64.b64encode(open(img_path, "rb").read())
    decoded = encoded.decode("ascii")

    # load .json file
    json_path = LABELME_PATH + f'{i}.json'
    json_file = open(json_path, 'r')
    json_object = json.load(json_file)
    json_file.close()

    if decoded == None:
      raise ValueError(f"Decoding image data from {img_path} failed")
    json_object['imageData'] = decoded

    # dump json object to file
    json_file = open(json_path, 'w')
    json.dump(json_object, json_file)
    json_file.close()
  except:
    print(f"FAILED TO SAVE imageData FROM {i}.png TO {i}.json")
