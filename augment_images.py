# -*- coding: utf-8 -*-

import imageio
import imgaug as ia
import numpy as np
from imgaug.augmentables.kps import KeypointsOnImage
from imgaug.augmentables.polys import Polygon
from imgaug.augmentables.segmaps import SegmentationMapsOnImage
import imgaug.augmenters as iaa
import sys
import time
#%matplotlib inline

input_path = 'tmp/margins/'
output_path = 'tmp/augmented/'
margin = int(sys.argv[1])
N_images = 3*int(sys.argv[2])
visualize = False

time_start = time.time()

for i in range(1, N_images+1):
  image = imageio.imread(f'{input_path}MarkerMargins_{((i-1)%20)+1}.png')

  kps_xy = np.float32([
    [margin, margin], # left bottom
    [margin, image.shape[0]-margin],  # left top 
    [image.shape[1]-margin, image.shape[0]-margin],  # right top
    [image.shape[1]-margin, margin]  # right bottom
  ])

  kpsoi = KeypointsOnImage.from_xy_array(kps_xy, shape=image.shape)
  polygon = Polygon(kpsoi.keypoints)

  # converting Polygon instances to segmentation map
  # 1. draw polygon to np.array as if it were an image
  segmap = np.zeros((image.shape[0], image.shape[1], 3), dtype=np.uint8)
  segmap = polygon.draw_on_image(
      segmap,
      color=(0, 255, 0),
      alpha=1.0, alpha_lines=0.0, alpha_points=0.0)
  segmap = np.argmax(segmap, axis=2)
  segmap = segmap.astype(np.uint8) #segmap.astype(np.int32)

  # convert array to SegmentationMapsOnImage instance
  segmap = SegmentationMapsOnImage(segmap, shape=image.shape)
  #ia.imshow(segmap.draw_on_image(image)[0])

  seq = iaa.Sequential([
      iaa.Add((-90, 90), per_channel=0),
      iaa.Add((-30, 30), per_channel=0.4),
      iaa.GammaContrast((0.5, 2.0)),
      iaa.CoarseDropout(0.1, size_percent=0.2),
      iaa.PerspectiveTransform(scale=(0.01, 0.167)),
      iaa.Affine(rotate=(-45, 45)),
      iaa.MotionBlur(k=15),
      iaa.Cutout(fill_mode="constant", cval=(0, 255), fill_per_channel=0.3, size=(0.05, 0.2)),
      iaa.Dropout(p=(0, 0.1), per_channel=0.8), 
      iaa.AdditiveGaussianNoise(scale=(0, 0.2*255))
      #iaa.ElasticTransformation(alpha=10, sigma=1)
  ])

  image_aug, segmap_aug = seq.augment(image=image, segmentation_maps=segmap)

  # visualize
  if visualize:
    ia.imshow(np.hstack([
        segmap_aug.draw_on_image(image_aug)[0],
        segmap_aug.draw()[0]
    ]))

  segmap_aug_drawn = segmap_aug.draw(size=image_aug.shape[:2])[0]
  segmap_aug_bw = np.where(segmap_aug_drawn>0, 255, 0).astype(np.uint8)
  imageio.imwrite(f'{output_path}/segmentations/{i}_ar_0.png', segmap_aug_bw)
  imageio.imwrite(f'{output_path}/images/{i}.png', image_aug)
  time_elapsed = time.ti() - time_start
  ETA = time_elapsed*(N_images/i - 1)
  print(f"\r CREATING AUGMENTATIONS -- PROGRESS: {int(100*i/N_images)}% -- ETA: {ETA/60:.0f} min", end='')
print("")

# ADD RAW (NON-AUGMENTED) IMAGES TO THE DATASET
for i in range(1, int(N_images*0.04)):
  image = imageio.imread(f'{input_path}MarkerMargins_{((i-1)%20)+1}.png')

  kps_xy = np.float32([
    [margin, margin], # left bottom
    [margin, image.shape[0]-margin],  # left top
    [image.shape[1]-margin, image.shape[0]-margin],  # right top
    [image.shape[1]-margin, margin]  # right bottom
  ])

  kpsoi = KeypointsOnImage.from_xy_array(kps_xy, shape=image.shape)
  polygon = Polygon(kpsoi.keypoints)

  # converting Polygon instances to segmentation map
  # 1. draw polygon to np.array as if it were an image
  segmap = np.zeros((image.shape[0], image.shape[1], 3), dtype=np.uint8)
  segmap = polygon.draw_on_image(
      segmap,
      color=(0, 255, 0),
      alpha=1.0, alpha_lines=0.0, alpha_points=0.0)
  segmap = np.argmax(segmap, axis=2)
  segmap = segmap.astype(np.int32)

  # convert array to SegmentationMapsOnImage instance
  segmap = SegmentationMapsOnImage(segmap, shape=image.shape)
  #ia.imshow(segmap.draw_on_image(image)[0])

  segmap_drawn = segmap.draw(size=image.shape[:2])[0]
  segmap_bw = np.where(segmap_drawn>0, 255, 0).astype(np.uint8)
  imageio.imwrite(f'{output_path}/segmentations/{N_images+i}_ar_0.png', segmap_bw)
  imageio.imwrite(f'{output_path}/images/{N_images+i}.png', image)
  print(f"\r ADDING SOME RAW IMAGES -- PROGRESS: {int(100*i/N_images*0.04)}%", end='')
print("")
