#!/bin/bash

rm -r tmp/
rm -r Output/
mkdir Output
mkdir tmp
mkdir tmp/margins
mkdir tmp/augmented
mkdir tmp/augmented/images
mkdir tmp/augmented/segmentations
mkdir tmp/copypaste_input
mkdir tmp/copypaste_input/foregrounds
mkdir tmp/copypaste_input/backgrounds
python add_margins_2_images.py $1


for ((i=0; i<$2; i++))
do
   echo "Batch $((i+1)) of $2"
   python augment_images.py $1 100
   python png2coco.py
   python coco2labelme.py
   python add_imageData.py
   python create_input_images_from_labelme.py --input_dir tmp/augmented/labelme/ --output_dir tmp/copypaste_input/foregrounds --done_images $((i*100))
   rm -r tmp/augmented
   mkdir tmp/augmented
   mkdir tmp/augmented/images
   mkdir tmp/augmented/segmentations
done

mv Input/foregrounds tmp/foregrounds
cp -r tmp/copypaste_input/foregrounds Input
python data_generation.py --input_dir Input/ --output_dir Output/ --image_number $(($2*100)) --scaling_factors 0.07 0.3
rm -r Input/foregrounds
mv tmp/foregrounds Input
python labelme2coco.py --output Output/coco/coco.json Output
rm -rf -d Output/*.json
